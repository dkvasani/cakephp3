<?php ?>
<!-- File: src/Template/Articles/edit.ctp -->

<div class="users form">
<?php echo $this->Form->create($user) ?>
    <fieldset>
        <legend><?php echo __('Add User') ?></legend>
        <?php echo $this->Form->input('username') ?>
        <?php echo $this->Form->input('email') ?>
        <?php echo $this->Form->input('password', array('default'=>false ,'value' => '','required' => false)) ?>
        <?php echo $this->Form->input('confirm_password',array('type' => 'password','required' => false)) ?>
        <?php echo $this->Form->input('role', [
            'options' => ['admin' => 'Admin', 'author' => 'Author']
        ]) ?>
    </fieldset>
<?php echo $this->Form->button(__('Submit')); ?>
<?php echo $this->Form->end() ?>
</div>