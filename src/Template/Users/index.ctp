<?php
$session = $this->request->session();
echo $name = $session->read('Auth.User.email');
echo '<pre>';
print_r($session->read());
?>
<h1>Users</h1>
<p><?php echo $this->Html->link("Add User", ['action' => 'add']) ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Role</th>
        <th>Created</th>
    </tr>

<!-- Here's where we iterate through our $users query object, printing out article info -->

<?php  foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user->id; ?></td>
        <td>
            <?php echo $this->Html->link($user->username, ['action' => 'view', $user->id]); ?>
        </td>
        <td>
            <?php echo $this->Html->link($user->email, ['action' => 'view', $user->id]); ?>
        </td>
        <td>
            <?php echo $user->role; ?>
        </td>
        <td>
            <?php //echo $user->created->format(DATE_RFC850); ?>
        </td>
        <td>
            <?php echo $this->Html->link('Edit', ['action' => 'edit', $user->id]); ?>
            <?php echo $this->Html->link('Delete', ['action' => 'delete', $user->id]); ?>
        </td>
        
    </tr>
<?php endforeach; ?>

</table>