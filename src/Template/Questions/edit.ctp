<?php
?>
<!-- File: src/Template/Question/edit.ctp -->

<h1>Edit Question</h1>
<?php
    echo $this->Form->create($question);
    echo $this->Form->input('question', ['rows' => '3']);
    echo $this->Form->button(__('Save Question'));
    echo $this->Form->end();
?>