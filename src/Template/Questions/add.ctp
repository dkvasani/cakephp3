<?php
?>

<!-- File: src/Template/Articles/add.ctp -->

<h1>Add Questions</h1>
<?php
    echo $this->Form->create($questions);
    echo $this->Form->input('Question.question', ['rows' => '5']);
    echo $this->Form->input('Answer.answer', ['rows' => '5','name' => 'answer[]','class' => 'ans-textarea' ,'label' => 'Answer 1']);
?>
<p><?php echo $this->Html->link("Add More Answer", '#', array('class' => 'add-more-answer')) ?></p>
<?php    echo $this->Form->button(__('Save Question'));
    echo $this->Form->end();
?>
<script type="text/javascript">
    
    $(document).ready(function () {
        click = 1;
        $('.add-more-answer').click(function(){
            click++;
            $('.ans-textarea').after('Answer ' + click + '<textarea name="answer[]" rows="5" required="required" maxlength="11"></textarea>');
        });
    });
</script>