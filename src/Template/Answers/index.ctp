<h1>Blog articles</h1>
<p><?= $this->Html->link("Add Question", ['action' => 'add']) ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

<!-- Here's where we iterate through our $questions query object, printing out article info -->

<?php foreach ($questions as $question): ?>
    <tr>
        <td><?= $question->id ?></td>
        <td>
            <?= $this->Html->link($question->question, ['action' => 'view', $question->id]) ?>
        </td>
        <td>
            <?= $question->created->format(DATE_RFC850) ?>
        </td>
        <td>
            <?= $this->Html->link('Edit', ['action' => 'edit', $question->id]) ?>
        </td>
    </tr>
<?php endforeach; ?>

</table>