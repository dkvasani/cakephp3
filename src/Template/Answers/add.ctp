
<h1>Add Answers</h1>
<?php
echo $this->Form->create($answers);
echo $this->Form->input('question_id', array(
    'options' => $questionsArr
));
echo $this->Form->input('answer', ['rows' => '5','name' => 'answer[]'],array('class' => 'textarea', 'label' => 'Answer'));
?>
<p><?php echo $this->Html->link("Add More Answer", '#', array('class' => 'add-more-answer')) ?></p>
<?php
echo $this->Form->button(__('Save Answers'));
echo $this->Form->end();
?>
<script type="text/javascript">

    
    $(document).ready(function () {
        $('.add-more-answer').click(function(){
            $('.textarea').append('<textarea name="answer[]" rows="5" required="required" maxlength="11"></textarea>');
        });
    });
</script>