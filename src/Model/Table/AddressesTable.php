<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AddressesTable extends Table
{
    
    public function initialize(array $config)
    {
           $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
        ]);
    }
    
    public function validationDefault(Validator $validator)
    {
//        $validator
//            ->notEmpty('title')
//            ->notEmpty('body');
//
//        return $validator;
    }
    
}