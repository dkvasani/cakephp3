<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class QuestionsTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
         $this->hasMany('Answers', [
            'foreignKey' => 'question_id',
            'dependent' => true,
        ]);
        //$this->belongsTo('Questions');
    }
    
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('question');

        return $validator;
    }
    
}