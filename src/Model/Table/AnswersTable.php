<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AnswersTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        //$this->belongsTo('Questions');
    }
    
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('answer');

        return $validator;
    }
    
}