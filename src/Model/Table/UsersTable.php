<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
//use Cake\ORM\RulesChecker;

class UsersTable extends Table {

    public function validationDefault(Validator $validator) {

        $validator->notEmpty('username', 'A username is required')
                ->add('username', 'length', [
                    'rule' => ['minLength', 6],
                    'message' => 'Username must be 6 characters or more',
                ])->add('username', [
            'unique' => ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'This Username is already used',]
        ]);

        $validator->requirePresence('password')->notEmpty('password', 'A password is required');


        $validator->requirePresence('confirm_password')->notEmpty('confirm_password', 'A password is required');
        $validator->add('confirm_password', 'compareWith', [
            'rule' => ['compareWith', 'password'],
            'message' => 'Passwords and confirm password are not equal.'
                ]
        );


        $validator->notEmpty('role', 'A role is required')
                ->add('role', 'inList', [
                    'rule' => ['inList', ['admin', 'author']],
                    'message' => 'Please enter a valid role'
        ]);

        $validator->notEmpty('email', 'A email is required')
                ->add('email', 'validFormat', [
                    'rule' => 'email',
                    'message' => 'E-mail must be valid'
                ])
                ->add('email', [
                    'unique' => ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'This Email is already used',]
        ]);
        return $validator;
    }

/** This function is used for only update validation
 * 
 * @param Validator $validator
 * @return Validator
 */    
    public function validationUpdate(Validator $validator)
    {
//        $validator
//            ->add('username', 'notEmpty', [
//                'rule' => 'notEmpty',
//                'message' => __('You need to provide a title'),
//            ])
//            ->add('role', 'notEmpty', [
//                'rule' => 'notEmpty',
//                'message' => __('A body is required')
//            ]);
        return $validator;
    }
    
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->belongsTo('Addresses', [
            'foreignKey' => 'id',
            'joinType' => 'LEFT',
        ]);
    }

}
