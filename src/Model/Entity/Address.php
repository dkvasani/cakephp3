<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class Address extends Entity
{

    // Make all fields mass assignable for now.
    protected $_accessible = [
        
    ];


    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}