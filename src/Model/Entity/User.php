<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{

    // Make all fields mass assignable for now.
    protected $_accessible = [
        'username'            => true,
        'email'            => true,
        'password'         => true,
        'role'          => true,
    ];



    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}