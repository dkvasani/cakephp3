<?php

namespace App\Controller;

class QuestionsController extends AppController {

    public function index() {
        $questions = $this->Questions->find('all');
        $this->set(compact('questions'));
    }

    public function view($id = null) {
        $article = $this->Articles->get($id);
        $this->set(compact('article'));
    }

    public function add() {
        $questions = $this->Questions->newEntity();
        if ($this->request->is('post')) {
      $postData = $this->request->data;
      print_r($postData);exit();
//             
//             if(!empty($postData['answer'])){
//                 foreach ($postData['answer'] as  $key => $value) {
//                     $answerArr = ['answer' => 'Third comment', 'id' => null];
//                 }
//             }
//             echo "<pre>";
//             print_r($postData);
//             exit();
//             $data = [
//            'title' => 'My title',
//            'body' => 'The text',
//            'comments' => [
//                ['body' => 'First comment', 'id' => 1],
//                ['body' => 'Second comment', 'id' => 2],
//            ]
//        ];
//        $entity = $articles->newEntity($data);
//
//        $newData = [
//            'comments' => [
//                ['body' => 'Changed comment', 'id' => 1],
//                ['body' => 'A new comment'],
//            ]
//        ];
//        $articles->patchEntity($entity, $newData);
//        $articles->save($article);
//
//            //$questions = $this->Questions->patchEntity($questions, $this->request->data);
//
            $data = [
                'question' => 'My Third Question',
                'answers' => [
                    ['answer' => 'Third comment', 'id' => null],
                    ['answer' => 'Third comment', 'id' => null],
                ]
            ];
            $questions = $this->Questions->newEntity($this->request->data, [
                'associated' => ['Answers']
            ]);
            $questions = $this->Questions->patchEntity($questions, $this->request->data);

            $this->Questions->save($questions);

            xdebug_break();
            // $this->Questions->save($questions, ['associated' => ['Answers']]);

            if ($this->Questions->save($questions, ['associated' => ['Answers']])) {
                $this->Flash->success(__('Your Questions has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your Questions.'));
        }
        $this->set('questions', $questions);
    }

    public function edit($id = null) {
        $question = $this->Questions->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Questions->patchEntity($question, $this->request->data);
            if ($this->Questions->save($question)) {
                $this->Flash->success(__('Your article has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your article.'));
        }

        $this->set('question', $question);
    }

}
