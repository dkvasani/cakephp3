<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Email\Email;
use Cake\Cache\Cache;

class UsersController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        //$this->Auth->allow('add');
    }

    public function index() {
        
        $query = $this->Users->find('all')->contain(['Addresses']);
        echo '<pre>';
        //print_r($query);
        foreach ($query as $address) {
           
            print_r($address->address);
            print_r($address->id);
            print_r($address->email);
            echo $address->Address;
        }
        exit();
        //$user = 'dkvasani';
        echo 'Logges in users Details<br> ID:' . $this->Auth->user('id');
        echo '<br>Username: ' . $this->Auth->user('username');
        echo '<br>Email: ' . $this->Auth->user('email');
        echo '<br>Role: ' . $this->Auth->user('role');
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id) {
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function add() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

    public function edit($id = null) {

        $user = $this->Users->get($id);

        if ($this->request->is(['post', 'put'])) {
            if (empty($this->request->data['password'])) {
                $this->request->data['password'] = $user['password'];
                $this->request->data['confirm_password'] = $user['password'];
            }
            $this->Users->patchEntity($user, $this->request->data);
            //$this->Users->patchEntity($user, $this->request->data,['validate' => 'update']);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('User has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your article.'));
        }

        $this->set('user', $user);
    }

    public function login() {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                /* creating custom session */
                $session = $this->request->session();
                $session->write('User', $user);

                /* Creating cache */
                if (($user = Cache::read('user')) === false) {
                    Cache::write('user', $user);
                }

                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(
                        __('Username or password is incorrect'), 'default', [], 'auth'
                );
            }
        }
    }

    public function logout() {
        $session = $this->request->session();
        $session->destroy();
        return $this->redirect($this->Auth->logout());
    }

    public function delete($id) {

        $users = $this->Users->get($id);
        if ($this->Users->delete($users)) {
            $this->Flash->success(__('The user with id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function sendMail() {
        try {
            $email = new Email('default');
            $email->from(['dharmesh.vasani@multidots.in' => 'Cakephp3 Site'])
                    ->to('dharmesh.vasani@multidots.in')
                    ->subject('About')
                    ->send('My message');
        } catch (Exception $e) {
            echo 'Exception : ', $e->getMessage(), "\n";
        }
        exit();
    }

    public function cacheData() {
        if ((Cache::read('user')) === false) {
            Cache::write('user', 'dkvasani');
        }
        $userData = array();
        Cache::clear('userData');
        if ((Cache::read('userData')) === false) {
            $users = $this->Users->find('all');
            foreach ($users as $key => $value) {
                $userData[] = array(
                            'email' => $value->email,
                            'username' => $value->username,
                            'role' => $value->role,
                    
                );
            }
            Cache::write('userData', $userData);
        }
        $user = Cache::read('user');
        $this->set('user', $user);
        $this->set('userData', Cache::read('userData'));
    }

}
