<?php

namespace App\Controller;

class ArticlesController extends AppController {

    public function index() {
        $articles = $this->Articles->find('all');

//        $result  = $this->Articles->find()
//                ->hydrate(true)
//                ->join([
//            'c' => [
//                'table' => 'users',
//                'type' => 'LEFT',
//                'conditions' => [
//                    'c.id = Articles.user_id'
//                ]
//            ],
//                ]);
//        echo '<pre>';
//        $dkv = $result->execute();
//        print_r($dkv);
        $articles = $this->Articles->find('all')->where(['user_id is != 0']);
                echo '<pre>';
               print_r($articles);
        exit();
        //where condtion in query
        $article = $this->Articles
                ->find()
                ->where(['id' => 1])
                ->first();
        var_dump($article->title);

        // Use the extract() method from the collections library
        // This executes the query as well
        $allTitles = $this->Articles->find()->extract('title');

        foreach ($allTitles as $title) {
            echo $title;
            echo "<br>";
        }

        $list = $this->Articles->find('list')->select(['id', 'title']);

        foreach ($list as $id => $title) {
            echo "$id : $title";
            echo "<br>";
        }

        $keyValueList = $this->Articles->find()->combine('id', 'title');
        print_r($keyValueList);

        $this->set(compact('articles'));
    }

    public function view($id = null) {
        $article = $this->Articles->get($id);
        $this->set(compact('article'));
    }

    public function add() {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your article.'));
        }
        $this->set('article', $article);
    }

    public function edit($id = null) {
        $article = $this->Articles->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your article.'));
        }

        $this->set('article', $article);
    }

}
