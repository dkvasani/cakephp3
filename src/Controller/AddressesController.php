<?php

namespace App\Controller;

class AddressesController extends AppController {

    public function index() {
        // In a controller or table method.
        
        $query = $this->Addresses->find('all')->contain(['Users']);
        echo '<pre>';
        //print_r($query);
        foreach ($query as $address) {
           
            //print_r($address);
            print_r($address->user->id);
            echo $address->city;
        }
        exit();
        $this->set(compact('articles'));
    }

    public function view($id = null) {
        $article = $this->Articles->get($id);
        $this->set(compact('article'));
    }

    public function add() {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your article.'));
        }
        $this->set('article', $article);
    }

    public function edit($id = null) {
        $article = $this->Articles->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your article.'));
        }

        $this->set('article', $article);
    }

}
