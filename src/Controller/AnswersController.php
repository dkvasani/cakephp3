<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class AnswersController extends AppController {

    public function index() {
        $answers = $this->Answers->find('all');
        $this->set(compact('answers'));
    }

    public function view($id = null) {
        $article = $this->Answers->get($id);
        $this->set(compact('answers'));
    }

    public function add() {
        $answers = $this->Answers->newEntity();
        if ($this->request->is('post')) {
            xdebug_break();
            $answerss = TableRegistry::get('Answers');
            $ansQuery = $answerss->query();
            $ansQuery->insert(['answer', 'question_id', 'created', 'modified']);
            if (!empty($this->request->data['answer'])) {
                foreach ($this->request->data['answer'] as $answer) {
                    $ansQuery->values(array(
                        'answer' => $answer,
                        'question_id' => $this->request->data['question_id'],
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ));
                }
            }

            if ($ansQuery->execute()) {
                $this->Flash->success(__('Your Answers has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your Answers.'));
        }
        $questions = TableRegistry::get('Questions');
        $query = $questions->find();
        $questionsArr = array();
        foreach ($query as $row) {
            $questionsArr[$row->id] = $row->question;
        }
        $this->set('answers', $answers);
        $this->set('questionsArr', $questionsArr);
    }

    public function edit($id = null) {
        $answer = $this->Answers->get($id);
        if ($this->request->is(['post', 'put'])) {
            $this->Answers->patchEntity($answer, $this->request->data);
            if ($this->Answers->save($answer)) {
                $this->Flash->success(__('Your answer has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your answer.'));
        }

        $this->set('answer', $answer);
    }

}
